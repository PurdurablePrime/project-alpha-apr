from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from projects.models import Project
from tasks.models import Task


@login_required
def create_task(request):
    project_id = request.GET.get("project_id")
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            if project_id:
                task.project = get_object_or_404(Project, id=project_id)
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm(
            initial={"project": project_id} if project_id else None
        )
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/mine.html", context)
