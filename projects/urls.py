from django.urls import path
from . import views
from projects.views import show_project, create_project


urlpatterns = [
    path("", views.list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
